<?php

namespace AppBundle\Repository;

use AppBundle\Service\Cache\Cache;

class CustomerCacheProxy extends Customer
{
    private $customer;
    private $cache;
    private $cacheKey = 'customers';

    public function __construct(
        Customer $customer,
        Cache $cache
    ) {
        $this->customer = $customer;
        $this->cache = $cache;
    }

    public function getAll()
    {
        $customers = $this->cache->get($this->cacheKey);
        if ( ! empty($customers)) {
            return unserialize($customers);
        }

        $customers = $this->customer->getAll();
        $this->cache->set($this->cacheKey, serialize($customers));

        return $customers;
    }

    public function persistMany($data)
    {
        $this->customer->persistMany($data);
        $this->cache->del($this->cacheKey);
    }

    public function deleteAll()
    {
        $this->customer->deleteAll();
        $this->cache->del($this->cacheKey);
    }
}