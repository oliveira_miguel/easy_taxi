<?php

namespace AppBundle\Repository;

use AppBundle\Service\Database\Database;

class Customer
{
    private $database;
    private $collection = 'customers';

    public function __construct(Database $database) {
        $this->database = $database;
    }

    public function getAll()
    {
        $customers = $this->database->find($this->collection);
        return $customers;
    }

    public function persistMany($data)
    {
        foreach ($data as $customer) {
            $this->database->insert($this->collection, $customer);
        }
    }

    public function deleteAll()
    {
        $this->database->drop($this->collection);
    }
}