<?php

namespace AppBundle\Tests;

trait DatabaseHelperTrait
{
    private $database;

    public function initDatabase()
    {
        $host = static::$kernel->getContainer()->getParameter('database_host');
        $port = static::$kernel->getContainer()->getParameter('database_port');
        $database = static::$kernel->getContainer()->getParameter('database_name');

        $mongoClient = new \MongoClient("mongodb://$host:$port/");
        $this->database = $mongoClient->selectDB($database);
    }

    public function insertInCollection($collection, $data)
    {
        $this->database->$collection->insert($data);
    }

    public function dropCollection($collection)
    {
        $this->database->$collection->drop();
    }

    public function hasInCollection($collection, $find)
    {
        $result = $this->database->$collection->find($find);
        return $result->count() > 0;
    }

    public function countInCollection($collection)
    {
        return $this->database->$collection->count();
    }
}