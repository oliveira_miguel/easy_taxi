<?php

namespace AppBundle\Tests\Service\Repository;

use AppBundle\Repository\CustomerCacheProxy;
use AppBundle\Repository\Customer;
use AppBundle\Service\Cache\Cache;

class CustomerCacheProxyTest extends \PHPUnit_Framework_TestCase
{
    public function test_get_all_should_not_hit_database_when_data_exists_in_cache()
    {
        $dataSerialized = serialize('test');

        $customerRepository = $this->getCustomerRepositoryMock();
        $customerRepository->expects($this->never())
             ->method('getAll');

        $cache = $this->getMock(Cache::class);
        $cache->method('get')
              ->will($this->returnValue($dataSerialized));

        $customerRepositoryProxy = new CustomerCacheProxy($customerRepository, $cache);
        $result = $customerRepositoryProxy->getAll();

        $this->assertEquals('test', $result);
    }

    public function test_get_all_should_hit_database_when_data_is_not_exists_in_cache()
    {
        $customerRepository = $this->getCustomerRepositoryMock();
        $customerRepository->expects($this->once())->method('getAll');

        $cache = $this->getMock(Cache::class);
        $cache->expects($this->once())->method('set');

        $customerRepositoryProxy = new CustomerCacheProxy($customerRepository, $cache);
        $result = $customerRepositoryProxy->getAll();
    }

    public function test_should_delete_cache_when_persist_customers()
    {
        $customerRepository = $this->getCustomerRepositoryMock();

        $cache = $this->getMock(Cache::class);
        $cache->expects($this->once())->method('del');

        $customerRepositoryProxy = new CustomerCacheProxy($customerRepository, $cache);
        $result = $customerRepositoryProxy->persistMany([]);
    }

    public function test_should_delete_cache_when_delete_customers()
    {
        $customerRepository = $this->getCustomerRepositoryMock();

        $cache = $this->getMock(Cache::class);
        $cache->expects($this->once())->method('del');

        $customerRepositoryProxy = new CustomerCacheProxy($customerRepository, $cache);
        $result = $customerRepositoryProxy->deleteAll();
    }

    private function getCustomerRepositoryMock()
    {
        return $this->getMockBuilder(Customer::class)
                ->disableOriginalConstructor()
                ->getMock();
    }
}