<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CustomersControllerTest extends WebTestCase
{
    use \AppBundle\Tests\DatabaseHelperTrait;

    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects();
        $this->initDatabase();

        $this->insertInCollection('customers', ['name' => 'Leandro', 'age' => 26]);
        $this->insertInCollection('customers', ['name' => 'Marcelo', 'age' => 30]);
    }

    public function test_create_error_with_no_customer()
    {
        $customers = [];
        $customers = json_encode($customers);
        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);

        $this->assertTrue($this->client->getResponse()->isClientError());
    }

    public function test_create_customers()
    {
        $customers = [
            ['name' => 'Miguel', 'age' => 23],
            ['name' => 'Test', 'age' => 100]
        ];

        $customers = json_encode($customers);
        $this->client->request('POST', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json'], $customers);

        $this->assertTrue($this->hasInCollection('customers', ['name' => 'Miguel']));
        $this->assertTrue($this->hasInCollection('customers', ['name' => 'Test']));
    }

    public function test_get_customers()
    {
        $this->client->request('GET', '/customers/', [], [], ['CONTENT_TYPE' => 'application/json']);
        $customers = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertEquals(2, count($customers));
    }

    public function test_delete_customers()
    {
        $this->client->request('DELETE', '/customers/');
        $this->assertTrue($this->client->getResponse()->isSuccessful());

        $this->assertEquals(0, $this->countInCollection('customers'));
    }

    public function tearDown()
    {
        $this->dropCollection('customers');
    }
}
