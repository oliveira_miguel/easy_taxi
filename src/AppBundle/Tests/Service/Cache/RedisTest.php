<?php

namespace AppBundle\Tests\Service\Cache;

use AppBundle\Service\Cache\Redis;

class RedisTest extends \PHPUnit_Framework_TestCase
{
    public function test_get_should_return_stored_data()
    {
        $redisService = $this->getRedisService();
        $result = $redisService->get('test');

        $this->assertEquals('some data', $result);
    }

    public function test_set_should_return_true_when_no_error()
    {
        $redisService = $this->getRedisService();
        $result = $redisService->set('test', 'test');

        $this->assertEquals(true, $result);
    }

    public function test_del_should_return_true_when_no_error()
    {
        $redisService = $this->getRedisService();
        $result = $redisService->del('test');

        $this->assertEquals(true, $result);
    }

    public function test_get_should_return_an_empty_array_when_exception()
    {
        $redisService = $this->getBrokenRedisService();
        $result = $redisService->get('test');

        $this->assertEquals([], $result);
    }

    public function test_set_should_return_false_when_exception()
    {
        $redisService = $this->getBrokenRedisService();
        $result = $redisService->set('test', 'test');

        $this->assertEquals(false, $result);
    }

    public function test_del_should_return_false_when_exception()
    {
        $redisService = $this->getBrokenRedisService();
        $result = $redisService->del('test');

        $this->assertEquals(false, $result);
    }

    private function getRedisService()
    {
        $redisClient = $this->getMock(\Predis\Client::class, ['get', 'set', 'del']);
        $redisClient->method('get')
            ->will($this->returnValue('some data'));

        return new Redis($redisClient);
    }

    private function getBrokenRedisService()
    {
        $exception = $this->getMockForAbstractClass(\Predis\PredisException::class);

        $redisClient = $this->getMock(\Predis\Client::class, ['get', 'set', 'del']);
        $redisClient->method('get')
            ->will($this->throwException($exception));

        $redisClient->method('set')
            ->will($this->throwException($exception));

        $redisClient->method('del')
            ->will($this->throwException($exception));

        return new Redis($redisClient);
    }
}