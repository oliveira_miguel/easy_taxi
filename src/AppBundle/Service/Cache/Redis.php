<?php

namespace AppBundle\Service\Cache;

use Predis\Client;

class Redis implements Cache
{
    private $client;

    public function __construct(Client $redis)
    {
        $this->client = $redis;
    }

    public function get($key)
    {
        try {
            return $this->client->get($key);
        } catch (\Predis\PredisException $e) {
            return [];
        }
    }

    public function set($key, $value)
    {
        try {
           $this->client->set($key, $value);
           return true;
        } catch (\Predis\PredisException $e) {
            return false;
        }
    }

    public function del($key)
    {
        try {
            $this->client->del($key);
            return true;
        } catch (\Predis\PredisException $e) {
            return false;
        }
    }
}