<?php

namespace AppBundle\Service\Cache;

use Predis;

interface Cache
{
    public function get($key);

    public function set($key, $value);

    public function del($key);
}