<?php

namespace AppBundle\Service\Database;

use \MongoClient;

class MongoDB implements Database
{
    protected $database;

    public function __construct($host, $port, $database)
    {
        $mongoClient = new MongoClient("mongodb://$host:$port/");
        $this->database = $mongoClient->selectDB($database);
    }

    public function insert($collection, $data)
    {
        $this->database->$collection->insert($data);
    }

    public function drop($collection)
    {
        $this->database->$collection->drop();
    }

    public function find($collection, $where = [])
    {
        $items = $this->database->$collection->find();
        return iterator_to_array($items);
    }
}
