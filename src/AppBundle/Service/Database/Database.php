<?php

namespace AppBundle\Service\Database;

interface Database
{
    public function insert($collection, $data);

    public function drop($collection);

    public function find($collection, $where = []);
}